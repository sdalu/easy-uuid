# -*- encoding: utf-8 -*-

require_relative "lib/uuid"

Gem::Specification.new do |s|
    s.name          = "easy-uuid"
    s.version       = UUID::VERSION
    s.platform      = Gem::Platform::RUBY
    s.summary       = "UUID library for Ruby"
    s.description   = "UUID library for Ruby"
    s.homepage      = "http://gitlab.com/sdalu/easy-uuid"
    s.license     = 'MIT'

    s.authors       = [ "Stephane D'Alu" ]
    s.email         = [ "sdalu@sdalu.com" ]

    s.files         = [ 'lib/uuid.rb' ]
    s.require_path  = 'lib'

    s.add_development_dependency 'yard', '~>0'
    s.add_development_dependency 'rake', '~>13'
end

