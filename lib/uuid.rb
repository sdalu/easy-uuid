require 'securerandom'
require 'digest/md5'
require 'digest/sha1'

module Kernel
    # Cast to UUID
    #
    # @raise [TypeError] 
    def UUID(uuid)
        if    uuid.nil?                       then UUID::NIL
        elsif uuid.is_a?(UUID)                then uuid
        elsif uuid.is_a?(Integer)             then UUID.new(uuid)
        elsif uuid = String.try_convert(uuid) then
            uuid.size == 16 ? UUID.new(uuid) : UUID.parse(uuid)
        else raise TypeError, "can't convert #{uuid.class} into UUID"
        end
    end
    module_function :UUID
end

class UUID
    # Version
    VERSION = '0.3.0'

    # Basic regex for validating UUID formatted string
    REGEX   = /\A\h{8}-\h{4}-\h{4}-\h{4}-\h{12}\z/


    # Parse a UUID formatted string
    #
    # @param str [String] string to parse
    #
    # @raise [ArgumentError] the string is not parsable as a UUID
    #
    # @return [UUID]
    #
    def self.parse(str)
        unless str =~ REGEX
            raise ArgumentError, "unable to parse UUID value"
        end

        self.new( [str.delete('-')].pack('H32') )
    end

    def self.create_v3(namespace, name)
        self.create_v3_v5(Digest::MD5,  '3', namespace, name)
    end

    def self.generate_v4
        UUID(SecureRandom.uuid)
    end
    
    def self.create_v5(namespace, name)
        self.create_v3_v5(Digest::SHA1, '5', namespace, name)
    end
    
    
    # Create UUID
    #
    # @param [String,Integer] UUID value
    #
    # @raise [ArgumentError] given integer is too big or
    #                        given string is not 16-byte 8bit
    #
    def initialize(v)
        case v
        when Integer
            hex = "%032x" % [ v ]
            if hex.size > 32
                raise ArgumentError,
                      "integer to big (must fit in 128-bit)"
            end
            @raw = [ hex ].pack('H32')
        when String
            if v.size != 16 || v.encoding.name != 'ASCII-8BIT'
                raise ArgumentError,
                    "need to be a 16 byte ASCII-8BIT string (#{v})"
            end
            @raw = v.dup
        else
            raise ArgumentError,
                  "expected 128-bit integer or 16-byte string"
        end
        @raw.freeze
    end


    # Return the 16-byte sequence forming the UUID.
    #
    # @return [Array<Integer>]
    #
    def bytes
        @raw.bytes
    end


    # Generates an integer hash value
    #
    # @return [Integer] hash value
    #
    def hash
        @raw.hash
    end
    def ===(other)
        other = UUID(other) rescue nil
        !other.nil? && self.to_raw == other.to_raw
    end
    def eql?(other)
        !other.nil? && other.is_a?(UUID) && self.to_raw == other.to_raw
    end
    alias_method :==,        :eql?

    def to_raw          ; @raw					; end

    # Returns a string containing a human-readable representation
    # of this object
    #
    # @return [String]
    #
    def inspect
        "#<#{self.class}:#{to_s}>"
    end


    # Get the UUID value as an integer
    #
    # @return [Integer] UUID value
    #
    def to_i
        i, j = @raw.unpack('Q>Q>')
        i * 2**64 + j
    end
    alias :to_int :to_i


    # Get the UUID string representation
    #
    # @return [String] UUID string representation
    #
    def to_s
        @raw.unpack('H8H4H4H4H12').join('-')
    end
    alias_method :to_str,    :to_s

    
    # Get the URI has a URN in UUID namespace
    #
    # @return [String] Uniform Resource Name
    #
    def to_uri
        "urn:uuid:#{self}"
    end


    # Get the JSON represenation
    #
    # @return [String]
    #
    def to_json(*a)
        self.to_s.to_json(*a)
    end


    # Sequel
    def sql_literal(ds) ; '0x' + @raw.unpack('H32')[0] 		; end
    def to_blob         ; Sequel.blob(@raw)            		; end

    
    # Nil/Empty UUID
    NIL  = UUID('00000000-0000-0000-0000-000000000000')

    # UUID with all bit set.
    # @note It is not a valid UUID but can be usefull
    FFF  = UUID('ffffffff-ffff-ffff-ffff-ffffffffffff')

    # Known namespace for version 3 and 5
    DNS  = UUID('6ba7b810-9dad-11d1-80b4-00c04fd430c8')
    URL  = UUID('6ba7b811-9dad-11d1-80b4-00c04fd430c8')
    OID  = UUID('6ba7b812-9dad-11d1-80b4-00c04fd430c8')
    X500 = UUID('6ba7b814-9dad-11d1-80b4-00c04fd430c8')


    def self.create_v3_v5(h, v, namespace, name)
        namespace_str   = namespace.to_raw
        name_str        = name.to_s
        digest          = h.hexdigest(namespace_str + name_str)
        UUID.new([digest.tap {|s| s[12] = v }].pack('H32'))
    end

    
    private_class_method :create_v3_v5

    
end
